<?php

/**
 * @file
 *  Token by type, administration pages.
 */

/**
 * Administration page.
 */
function tokenbytype_admin() {
  $tokens = variable_get('tokenbytype_tokens', array());

  $header = array(
    array('data' => t('Title')),
    array('data' => t('Description')),
    array('data' => ''),
    array('data' => ''),
  );
  $destination = drupal_get_destination();
  $rows = array();
  foreach ($tokens as $token) {
    $rows[] = array(
      $token['name'],
      check_plain($token['description']),
      l(t('edit'), 'admin/settings/tokenbytype/' . $token['name'] . '/edit', array('query' => $destination)),
      l(t('delete'), 'admin/settings/tokenbytype/' . $token['name'] . '/delete', array('query' => $destination)),
    );
  }
  if (! count($rows)) {
    $rows[] = array('data' => array(array('data' => t('No custom tokens yet'), 'colspan' => 4)));
  }

  $output = theme('table', $header, $rows);
  $output .= '<p>' . l(t('Add a token'), 'admin/settings/tokenbytype/add') . '</p>';

  return $output;
}

/**
 * Implementation of hook_form.
 *
 * Add or edit a token.
 *
 * @see tokenbytype_token_form_submit().
 * @see tokenbytype_token_form_validate().
 */
function tokenbytype_token_form(&$form_state, $token_values) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Token name'),
    '#default_value' => (! empty($token_values['name'])) ? $token_values['name'] : '',
    '#maxlength' => 30,
    '#size' => 30,
    '#description' => t('Up to 30 characters to be used for token substitution. Using only lowercase a-z and hyphen -.'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => (! empty($token_values['description'])) ? check_plain($token_values['description']) : '',
    '#description' => t('To be shown in token lists, like below.'),
  );

  foreach (node_get_types('types') as $type) {
    $form[$type->type] = array(
      '#title' => drupal_ucfirst($type->name),
      '#type' => 'textarea',
      '#default_value' => (! empty($token_values[$type->type])) ? $token_values[$type->type] : '',
      '#element_validate' => array('token_element_validate'),
      '#token_types' => array('node'),
      '#description' => t('Substitution for token for the content type.'),
    );
  }
  $form['default'] = array(
    '#title' => t('Default'),
    '#type' => 'textarea',
    '#default_value' => (! empty($token_values['default'])) ? $token_values['default'] : '',
    '#element_validate' => array('token_element_validate'),
    '#token_types' => array('node'),
    '#description' => t('Substitution for token if the content-type specific substitution is empty.'),
  );

  $form['sumbit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['help'] = array(
    '#value' => theme('token_help'),
  );


  return $form;
}

/**
 * Validate handler for tokenbytype_token_form().
 *
 * Additional to element validation for token replacement.
 */
function tokenbytype_token_form_validate($form, &$form_state) {
  if (! tokenbytype_name_validate($form_state['values']['name'])) {
    form_set_error('name', t('Requires token name with only characters a-z and -'));
  }
  foreach (node_get_types('types') as $type) {
    if (strpos($form_state['values'][$type->type], '[' . $form_state['values']['name'] . ']') !== FALSE) {
      form_set_error($type->type, t('The replacement must not include the token itself'));
    }
  }
  if (strpos($form_state['values']['default'], '[' . $form_state['values']['name'] . ']') !== FALSE) {
    form_set_error('default', t('The replacement must not include the token itself'));
  }
}

/**
 * Submit handler for tokenbytype_token_form().
 */
function tokenbytype_token_form_submit($form, &$form_state) {
  tokenbytype_save($form_state['values']['name'], $form_state['values']);
}

/**
 * Implementation of hook_form().
 *
 * @see tokenbytype_delete_confirm_submit().
 */
function tokenbytype_delete_confirm(&$form_state, $token) {
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $token['name'],
  );

  return confirm_form($form, t('Are you sure you want to delete token %name', array('%name' => '[' . $token['name'] . ']')));
}

/**
 * Submit handler for tokenbytype_delete_confirm().
 */
function tokenbytype_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    tokenbytype_delete($form_state['values']['name']);
  }
}
